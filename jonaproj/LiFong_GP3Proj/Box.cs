﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace LiFong_GP3Proj
{
    class Box
    {
        Texture2D box;
        public Rectangle boxRect;
        public Vector2 pos;

        public Box(Texture2D newbox, Vector2 newPos)
        {
            box = newbox;
            pos = newPos;

            boxRect = new Rectangle((int)pos.X, (int)pos.Y, box.Width, box.Height);

        }

        public Texture2D boxTexture
        {
            get
            {
                return box;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(box, boxRect, Color.White);
        }



    }
}
