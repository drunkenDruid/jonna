﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace LiFong_GP3Proj
{
    class Fence
    {
        Texture2D fence;
        public Rectangle fenceRect;
        public Vector2 pos;

        public Fence(Texture2D newfence, Vector2 newPos)
        {
            fence = newfence;
            pos = newPos;

            fenceRect = new Rectangle((int)pos.X, (int)pos.Y, fence.Width, fence.Height);

        }

        public Texture2D fenceTexture
        {
            get
            {
                return fence;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(fence, fenceRect, Color.White);
        }



    }
}
