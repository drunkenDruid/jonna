﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiFong_GP3Proj
{
    class Tiger
    {
        public Texture2D tigerTexture;
        public Vector2 tigerPos = new Vector2(300, 0);
        public Vector2 tigerVelocity;
        public Rectangle tigerRectangle;
        public Color tigerColor;

        public bool checkJump = false;

        public Tiger(Texture2D tigerTexture, Rectangle tigerRectangle, Color tigerColor)
        {
            this.tigerTexture = tigerTexture;
            this.tigerRectangle = tigerRectangle;
            this.tigerColor = tigerColor;
        }

        public Vector2 Position

        {
            get
            {
                return tigerPos;
            }
        }

        public Texture2D TigerTexture
        {
            set
            {
                tigerTexture = value;
            }
            get
            {
                return tigerTexture;
            }
        }
        public Color TigerColor
        {
            set
            {
                tigerColor = value;
            }
            get
            {
                return tigerColor;
            }
        }


        public Rectangle TigerRectangle
        {
            set
            {
                tigerRectangle = value;
            }
            get
            {
                return tigerRectangle;
            }
        }

        public void Load(ContentManager Content)
        {
            tigerTexture = Content.Load<Texture2D>("tiger");
        }
        public void Update(GameTime gameTime)
        {
            tigerPos += tigerVelocity;
            tigerRectangle = new Rectangle((int)tigerPos.X, (int)tigerPos.Y, 40, 40);

            Input(gameTime);

            if (tigerVelocity.Y < 10)
            {
                tigerVelocity.Y += 0.4f;
            }

        }
        private void Input(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                tigerVelocity.X = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 8;

            }
            else if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                tigerVelocity.X = -(float)gameTime.ElapsedGameTime.TotalMilliseconds / 8;
            }
            else
            {
                tigerVelocity.X = 0f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space) && checkJump == false)
            {
                tigerPos.Y -= 5f;
                tigerVelocity.Y = -9f;
                checkJump = true;
            }
        }

        public void Collision(Rectangle newtigerRectangle, int xOffset, int yOffset)
        {
            if (tigerRectangle.TouchTopOf(newtigerRectangle))
            {
                tigerRectangle.Y = newtigerRectangle.Y - tigerRectangle.Height;
                tigerVelocity.Y = 0f;
                checkJump = false;
            }
            if (tigerRectangle.TouchLeftOf(newtigerRectangle))
            {
                tigerPos.X = newtigerRectangle.X - tigerRectangle.Width - 2;
            }
            if (tigerRectangle.TouchLeftOf(newtigerRectangle))
            {
                tigerPos.X = newtigerRectangle.X + newtigerRectangle.Width + 2;

            }
            if (newtigerRectangle.TouchBottomOf(newtigerRectangle))
            {
                tigerVelocity.Y = 1f;
            }
            if (tigerPos.X < 0)
            {
                tigerPos.X = 0;
            }
            if (tigerPos.X > xOffset - tigerRectangle.Width)
            {
                tigerPos.X = xOffset - tigerRectangle.Width;
            }
            if (tigerPos.Y < 0)
            {
                tigerPos.X = 0;
            }
            if (tigerPos.Y > yOffset - tigerRectangle.Height)
            {
                tigerPos.Y = yOffset - tigerRectangle.Height;
            }
        }
    }
}
