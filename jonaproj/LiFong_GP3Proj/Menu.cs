﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;

namespace LiFong_GP3Proj
{
    class Menu
    {
        public Texture2D menuBg, playButt, controlButt, optionButt;
        public Rectangle playRect, controlRect, optionRect;
        
        public Menu()
        {
            
            
        }
        public void LoadContent(ContentManager Content)
        {
            menuBg = Content.Load<Texture2D>("Bg\\Menu");
            
            playButt = Content.Load<Texture2D>("Bg\\Play");
            controlButt = Content.Load<Texture2D>("Bg\\Controls");
            optionButt = Content.Load<Texture2D>("Bg\\Options");
        }
        public void Update(GameTime gameTime)
        {
            playRect = new Rectangle(40, 50, playButt.Width, playButt.Height);
            controlRect = new Rectangle(42, 109, playButt.Width, playButt.Height);
            optionRect = new Rectangle(42, 168, playButt.Width, playButt.Height);
        }
        public void Draw(SpriteBatch spriteBatch)
        {

            spriteBatch.Draw(menuBg, Vector2.Zero, Color.White);
            spriteBatch.Draw(playButt, playRect, Color.White);
            spriteBatch.Draw(controlButt, controlRect, Color.White);
            spriteBatch.Draw(optionButt, optionRect, Color.White);


        }
    }
}
