﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace LiFong_GP3Proj
{
    class Platform
    {
        public Texture2D platform;
        public Rectangle platformRect;
       
        public Vector2 pos;

        public Platform(Texture2D newPlatform, Vector2 newPos)
        {
            platform = newPlatform;
            pos = newPos;

            platformRect = new Rectangle((int)pos.X, (int)pos.Y, platform.Width, platform.Height);

        }

        public Texture2D PlatformTexture
        {
            get
            {
                return platform;
            }
        }
       
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(platform, platformRect, Color.White);
        }
        //public void platformCollide(Rectangle r1)
        //{
        //    if(platformRect.Intersects(r1))
        //    {
        //        platformX = !platformX;
        //        platformY = !platformY;
        //    }
        //}
    }
}
