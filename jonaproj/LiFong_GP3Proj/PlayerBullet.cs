﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

namespace LiFong_GP3Proj
{
    class PlayerBullet
    {
        public Texture2D ball1;
        public Rectangle ballRect1;
        public Color ballColor1;
        public bool checkR, checkL, checkM;

        
        public PlayerBullet(Texture2D ball1, Rectangle ballRect1, Color ballColor1)
        {
            this.ball1 = ball1;
            this.ballRect1 = ballRect1;
            this.ballColor1 = ballColor1;

            checkR = false;
            checkM = false;
            checkL = false;
        }
        public Texture2D BallTexture
        {
            get
            {
                return ball1;
            }
        }

        public Rectangle BallRectangle
        {
            get
            {
                return ballRect1;
            }
        }

        public Color BallColor
        {
            get
            {
                return ballColor1;
            }
        }

        public void bulletMove()
        {
            if(checkL)
            {
                ballRect1.Location = new Point(ballRect1.Location.X - 5, ballRect1.Location.Y);
            }
           
            if (checkR)
            {
                ballRect1.Location = new Point(ballRect1.Location.X + 5, ballRect1.Location.Y);
            }
        }

        public Point BulletLocation
        {
            set
            {
                ballRect1.Location = value;
            }
        }
    }
}
