﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace LiFong_GP3Proj
{
    class Table
    {
        Texture2D table;
        public Rectangle tableRect;
        public Vector2 pos;

        public Table(Texture2D newtable, Vector2 newPos)
        {
            table = newtable;
            pos = newPos;

            tableRect = new Rectangle((int)pos.X, (int)pos.Y, table.Width, table.Height);

        }

        
        public Texture2D tableTexture
        {
            get
            {
                return table;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(table, tableRect, Color.White);
        }
        
    }
}
