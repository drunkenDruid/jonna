﻿    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Xna.Framework;
    using Microsoft.Xna.Framework.Graphics;
    using Microsoft.Xna.Framework.Input;
    using Microsoft.Xna.Framework.Content;

    namespace LiFong_GP3Proj
    {
        class Stairs
        {
            Texture2D stair;
            public Rectangle stairRect;
            public Vector2 pos;

            public Stairs(Texture2D newstair, Vector2 newPos)
            {
                stair = newstair;
                pos = newPos;

                stairRect = new Rectangle((int)pos.X, (int)pos.Y, stair.Width, stair.Height);

            }

            public Texture2D stairTexture
            {
                get
                {
                    return stair;
                }
            }

            public void Draw(SpriteBatch spriteBatch)
            {
                spriteBatch.Draw(stair, stairRect, Color.White);
            }

        

        }
    }
