﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace LiFong_GP3Proj
{
    class BG
    {
        public Texture2D bgTexture;

        public void LoadContent(ContentManager Content)
        {
            bgTexture = Content.Load<Texture2D>("Bg//BG3");
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(bgTexture, Vector2.Zero, Color.White);
        }
    }
}
