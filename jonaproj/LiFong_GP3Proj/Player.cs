﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace LiFong_GP3Proj
{
    class Player
    {
        public Texture2D playerIdle, playerWR, playerWL, playerShtR, playerShtL, playerWRSht, playerWLSht, playerJmpR, playerJmpL;
        public Texture2D playerHitR, playerHitL, playerDeadR, playerDeadL;
        public Texture2D current;

        public bool checkWR, checkWL, checkSR, checkSL, checkWSR, checkWSL, checkJmpR, checkJmpL, checkHR, checkHL, 
                    checkDR, checkDL, checkJump, checkIdle;
          
        public Rectangle startRect, endRect;

        public float elapse;
        public float delay = 50f;
        public int frames;

        public KeyboardState key;
        public Vector2 pos = new Vector2(64, 0);

        public Vector2 currentHeight = new Vector2();
        public int maxHeight;
        public int currentHeight1, posY;

        public Vector2 playerVelocity;
      
        public Player()
        {
            checkWR = false;
            checkWL = false;
            checkSR = false;
            checkSL = false;
            checkWSR = false;
            checkWSL = false;
            checkJmpR = false;
            checkJmpL = false;
            checkHR = false;
            checkHL = false;
            checkDR = false;
            checkDL = false;
            checkIdle = true;

            checkJump = false;
            currentHeight1 = 0;
            currentHeight.X = 0;
            currentHeight.Y = 0;
            maxHeight = 30;
           
        }

        public void LoadContent(ContentManager Content)
        {
            playerIdle = Content.Load<Texture2D>("Images\\BoyIdle");
            playerWR = Content.Load<Texture2D>("Images\\BoyWalkR");
            playerWL = Content.Load<Texture2D>("Images\\BoyWalkL");
            playerShtR = Content.Load<Texture2D>("Images\\BoyShootR");
            playerShtL = Content.Load<Texture2D>("Images\\BoyShootL");
            playerWRSht = Content.Load<Texture2D>("Images\\BoyWalkShootR");
            playerWLSht = Content.Load<Texture2D>("Images\\BoyWalkShootL");
            playerJmpR = Content.Load<Texture2D>("Images\\BoyJumpR");
            playerJmpL = Content.Load<Texture2D>("Images\\BoyJumpL"); 

            playerHitR = Content.Load<Texture2D>("Images\\BoyWalkL");
            playerHitL = Content.Load<Texture2D>("Images\\BoyWalkL");
            playerDeadR = Content.Load<Texture2D>("Images\\BoyWalkL");
            playerDeadL = Content.Load<Texture2D>("Images\\BoyWalkL");
            
            current = playerIdle;

            //pos.X = 300;
            //pos.Y = 200;

        }

        private void Animate(GameTime gameTime)
        {
            elapse += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapse >= delay)
            {
                if (frames >= 8)
                {
                    frames = 0;
                }
                else
                {
                    frames++;
                }
                elapse = 0;
            }

            startRect = new Rectangle(88 * frames, 0, 88, 93);
        }
        public void Update(GameTime gameTime)
        {
            pos += playerVelocity;
            endRect = new Rectangle((int)pos.X, (int)pos.Y, 30, 30);

            Input(gameTime);
            Animate(gameTime);

            if (playerVelocity.Y < 5)
            {
                playerVelocity.Y += 0.6f;
            }


            
            endRect = new Rectangle((int) pos.X, (int)pos.Y, 88, 93);

        }
            public void Input(GameTime gameTime)
            {

                key = Keyboard.GetState();
                if (checkIdle)
                {
                    if (key.IsKeyUp(Keys.D) || (key.IsKeyUp(Keys.A)) ||
                    Mouse.GetState().MiddleButton == ButtonState.Pressed ||
                    Mouse.GetState().RightButton == ButtonState.Pressed ||
                    Mouse.GetState().LeftButton == ButtonState.Pressed)
                    {
                        current = playerIdle;

                    }
                }


                if (Keyboard.GetState().IsKeyDown(Keys.D))
                {
                    playerVelocity.X = (float)gameTime.ElapsedGameTime.TotalMilliseconds / 8;
                    current = playerWR;
                }
                else if (Keyboard.GetState().IsKeyDown(Keys.A))
                {
                    playerVelocity.X = -(float)gameTime.ElapsedGameTime.TotalMilliseconds / 8;
                    current = playerWL;
                }
                else
                {
                    playerVelocity.X = 0f;
                }
                if (Keyboard.GetState().IsKeyDown(Keys.Space) && checkJump == false)
                {
                    pos.Y -= 5f;
                    playerVelocity.Y = -9f;
                    checkJump = true;
                
                }



            if (Mouse.GetState().LeftButton == ButtonState.Pressed && !key.IsKeyDown(Keys.A) && !key.IsKeyDown(Keys.D))
            {
                current = playerShtR;
            }


            if (Mouse.GetState().LeftButton == ButtonState.Pressed && key.IsKeyDown(Keys.D))
            {
                current = playerWRSht;
            }
            if (Mouse.GetState().LeftButton == ButtonState.Pressed && key.IsKeyDown(Keys.A))
            {
                current = playerWLSht;
            }




        }
        public void Collision(Rectangle newendRect, int xOffset, int yOffset)
        {
            if (endRect.TouchTopOf(newendRect))
            {
                endRect.Y = newendRect.Y - endRect.Height;
                playerVelocity.Y = 0f;
                checkJump = false;
            }
            if (endRect.TouchLeftOf(newendRect))
            {
                pos.X = newendRect.X - endRect.Width - 2;
            }
            if (endRect.TouchLeftOf(newendRect))
            {
                pos.X = newendRect.X + newendRect.Width + 2;

            }
            if (newendRect.TouchBottomOf(newendRect))
            {
                playerVelocity.Y = 1f;
            }
            if (pos.X < 0)
            {
                pos.X = 0;
            }
            if (pos.X > xOffset - endRect.Width)
            {
                pos.X = xOffset - endRect.Width;
            }
            if (pos.Y < 0)
            {
                pos.X = 0;
            }
            if (pos.Y > yOffset - endRect.Height)
            {
                pos.Y = yOffset - endRect.Height;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {

            
            
                spriteBatch.Draw(current, endRect, startRect, Color.White );
           
        }

        public Point PlayerPos
        {
            set
            {
                endRect.Location = value;
            }
           

        }
        public Point getPlayerPos
        {
            get
            {
                return endRect.Location;
            }
        }


    }
}
