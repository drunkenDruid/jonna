﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiFong_GP3Proj
{
    class Floor
    {
        protected Texture2D floorTexture;
        private Rectangle floorRectangle;

        public Rectangle FloorRectangle
        {
            get
            {
                return floorRectangle;
            }
            protected set
            {
                floorRectangle = value;
            }
        }

        private static ContentManager content;

        public static ContentManager Content
        {
            protected get
            {
                return content;
            }
            set
            {
                content = value;
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(floorTexture, floorRectangle, Color.White);

        }
    }
}
