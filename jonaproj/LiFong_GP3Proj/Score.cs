﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiFong_GP3Proj
{
    class Score
    {
        public int score;
        public SpriteFont scoreFont;
        public Vector2 scorePos;
        public bool showHud;
        Player player = new Player();

        public Score()
        {
            score = 0;
            showHud = true;
            scoreFont = null;
            scorePos = new Vector2( );
        }
        public void LoadContent(ContentManager Content)
        {
            scoreFont = Content.Load<SpriteFont>("FONT");
            scorePos.X = player.endRect.X;
            scorePos.Y = player.endRect.Y;
        }
        public void Update(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();
            if(Keyboard.GetState().IsKeyDown(Keys.D))
            {
                scorePos.X += 2;
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            if (showHud)
                spriteBatch.DrawString(scoreFont, "Score - " + score, scorePos, Color.White);
        }
    }
}
