﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiFong_GP3Proj
{
    class EnemyElf1
    {
        public Texture2D enemyIdleR, enemyIdleL, enemyWR, enemyWL, enemyHitR, enemyHitL;
        public Texture2D enemyCurrent;

        public bool checkIdle, checkBorder;

        public Rectangle enemyStartRect, enemyEndRect;

        public float elapse;
        public float delay = 50f;
        public int frames;

        public KeyboardState key;
        public Vector2 pos = new Vector2();

        public SoundEffect bellSound;
        public SoundEffectInstance bellSoundControl;

        public int enemyCurrentHeight, maxHeight;

        public EnemyElf1()
        {
            
            checkIdle = true;
            checkBorder = false;

           
            
            enemyCurrentHeight = 0;
            maxHeight = 30;

        }

        public void LoadContent(ContentManager Content)
        {

            enemyIdleR = Content.Load<Texture2D>("Elf\\ElfFixR");
            enemyIdleL = Content.Load<Texture2D>("Elf\\ElfFixL");
            enemyWR = Content.Load<Texture2D>("Elf\\ElfWR");
            enemyWL = Content.Load<Texture2D>("Elf\\ElfWL");
            //enemyHitR = Content.Load<Texture2D>("Elf\\ElfWRHit");
            //enemyHitL = Content.Load<Texture2D>("Elf\\ElfWLHit");

            bellSound = Content.Load<SoundEffect>("Cat Bell");
            bellSoundControl = bellSound.CreateInstance();
           

            enemyCurrent = enemyWR;

            pos.X = 1000;
            pos.Y = 280;

        }

        private void Animate(GameTime gameTime)
        {
            elapse += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (elapse >= delay)
            {
                if (frames >= 11)
                {
                    frames = 0;
                }
                else
                {
                    frames++;
                }
                elapse = 0;
            }

            enemyStartRect = new Rectangle(85 * frames, 0, 85, 80);
        }
        
        public void Update(GameTime gameTime)
        {

           
            if (enemyEndRect.Location.X <= -1)
            {
                enemyCurrent = enemyWL;
                checkBorder = true;
            }
            if (enemyEndRect.Location.X >= 350)
            {
                enemyCurrent = enemyWR;
                checkBorder = true;
            }
            if (checkBorder)
            {
                enemyEndRect.Location = new Point(enemyEndRect.Location.X + 2, enemyEndRect.Location.Y);
            }
            else
            {
                enemyEndRect.Location = new Point(enemyEndRect.Location.X - 2, enemyEndRect.Location.Y);
            }
            if (enemyEndRect.Location.X >= 580)
            {
                enemyEndRect.Location = new Point(enemyEndRect.Location.X, 0);

            }
            

            Animate(gameTime);
            enemyEndRect = new Rectangle((int)pos.X, (int)pos.Y, 85, 93);

        }



        public void Draw(SpriteBatch spriteBatch)
        {
           

            spriteBatch.Draw(enemyCurrent, enemyEndRect, enemyStartRect, Color.White);

        }

        public Point enemyPos
        {
            set
            {
                enemyEndRect.Location = value;
            }


        }
        public Point getenemyPos
        {
            get
            {
                return enemyEndRect.Location;
            }
        }

    }
}
