﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiFong_GP3Proj
{
    class Camera
    {
        Matrix camPos;
        Player player = new Player();
        float speed;
       

        public Camera(Matrix camPos)
        {
            this.camPos = camPos;


            speed = 2f;
        }
       

        public void camMove(Keys key)
        {

            
            if (key == Keys.A )
            {
                camPos = Matrix.CreateTranslation(camPos.Translation.X + 1* speed, camPos.Translation.Y, camPos.Translation.Z);
            }
            if (key == Keys.D )
            {
                camPos = Matrix.CreateTranslation(camPos.Translation.X - 1 * speed, camPos.Translation.Y, camPos.Translation.Z);
            }
            
        }

        public void Update(GameTime gameTime)
        {
           
        }

        public Matrix getCamPos()
        {
            return camPos;

        }
    }
}
