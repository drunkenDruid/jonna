﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiFong_GP3Proj
{
    class Map
    {
        private List<CollisionFloor> collisionFloor = new List<CollisionFloor>();
        Player player = new Player();
        public List<CollisionFloor> CollisionFloor
        {
            get
            {
                return collisionFloor;
            }
        }

        private int width, height;
        public int Width
        {
            get
            {
                return width;
            }
        }
        public int Height
        {
            get
            {
                return height;
            }
        }

        public Map()
        {

        }
        public void Generate(int[,] map, int size)
        {
            size = 45;
            for (int x = 0; x < map.GetLength(1); x++)
            {
                for (int y = 0; y < map.GetLength(0); y++)
                {
                    int number = map[y, x];
                    if (number > 0)
                    {
                        collisionFloor.Add(new CollisionFloor(number, new Rectangle(x * size, y * size, size, size)));

                    }
                    width = (x + 1) * size;
                    height = (y + 1) * size;
                }
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (CollisionFloor floor in collisionFloor)
            {
                floor.Draw(spriteBatch);
            }
        }
    }
}
