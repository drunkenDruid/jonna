﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiFong_GP3Proj
{
   
        class CollisionFloor : Floor
        {
            public CollisionFloor(int i, Rectangle newfloorRectangle)
            {
                floorTexture = Content.Load<Texture2D>("Blocks//block" + i);
                this.FloorRectangle = newfloorRectangle;
            }
        }
    
}
