﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace LiFong_GP3Proj
{
    
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Player player = new Player();
        List<PlayerBullet> ball;
        public SoundEffect ballSound;
        public SoundEffectInstance ballSoundControl;

        int ballCount;
        bool ballDelay;

        EnemyElf1 enemy = new EnemyElf1();

        BG background = new BG();
        Score score = new Score();
      
        public Texture2D platformTexture;
        public Rectangle platformRect;


        Menu menu = new Menu();
        public Texture2D cursorTexture, gameOverTxt;
        public Rectangle cursorRect, gameOverRect;
        public Texture2D controlBg;
        public Rectangle controlBgRect;

       
        Camera camera;

        int pos;

        Map map;
        Tiger tora;
        Texture2D toraT;
        Rectangle toraR;
        public enum GameMode
        {
            Menu, InGame, GameOver, Controls,
        }
        GameMode gameMode1 = GameMode.Menu;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.PreferredBackBufferHeight = 400;
            graphics.PreferredBackBufferWidth = 700;
        }

        
        protected override void Initialize()
        {

            Matrix cameraPos = Matrix.CreateTranslation(player.getPlayerPos.X, player.getPlayerPos.Y, 0);
            camera = new Camera(cameraPos);

            cursorTexture = Content.Load<Texture2D>("Bg\\Cursor");
            cursorRect = new Rectangle(Mouse.GetState().X, Mouse.GetState().Y, cursorTexture.Width, cursorTexture.Height);

            gameOverTxt = Content.Load<Texture2D>("Bg\\GameOver");
            gameOverRect = new Rectangle(player.endRect.X, player.endRect.Y, gameOverTxt.Width, gameOverTxt.Height);

            controlBg = Content.Load<Texture2D>("Bg\\Controlspage");
           // controlBgRect = new Rectangle(player.endRect.X, player.endRect.Y, gameOverTxt.Width, gameOverTxt.Height);

            ball = new List<PlayerBullet>();
            ballCount = 3;
            ballDelay = false;
            ballSound = Content.Load<SoundEffect>("ball");
            ballSoundControl = ballSound.CreateInstance();

            //platformTexture = Content.Load<Texture2D>("Bg\\platform");
            map = new Map();

            base.Initialize();
        }

        
        protected override void LoadContent()
        {
            
            spriteBatch = new SpriteBatch(GraphicsDevice);
            player.LoadContent(Content);
            background.LoadContent(Content);
            score.LoadContent(Content);
            menu.LoadContent(Content);
            enemy.LoadContent(Content);

            Floor.Content = Content;

            map.Generate(new[,] {
                                    { 0, 0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,},//1
                                    { 0, 0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,},//2
                                    { 0, 0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,},//3
                                    { 0, 0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,},//4
                                    { 0, 0, 0, 0, 0, 0, 0, 1,    1, 1, 1, 1, 1, 0, 0,    1, 1, 1, 1, 1, 1, 1,   1, 1, 1, 1, 1, 1, 1,    1, 1, 0, 0, 0, 0, 3,    1, 1, 3, 0, 0, 0, 0,    1, 1, 1, 1, 1, 1, 0,    0, 0, 1, 0, 0, 1, 1,    0, 0, 0, 0, 0, 1, 1,    1, 1, 1, 1, 1, 1, 0,    0, 0, 0, 0, 0, 0, 0,    1, 1, 0, 0, 0, 1, 1,    1, 0, 0, 0, 0, 0, 0,},//5
                                    { 0, 0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 3, 4,    0, 0, 4, 3, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 3,    0, 0, 3, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,},//6
                                    { 0, 0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,   0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 3, 4, 4,    0, 0, 4, 4, 3, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 3, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,    0, 0, 4, 3, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,},//7
                                    { 0, 0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 0, 0, 2,    2, 0, 0, 2, 2, 0, 0,   0, 0, 0, 2, 2, 2, 0,    0, 0, 0, 3, 4, 4, 4,    0, 0, 4, 4, 4, 3, 0,    0, 0, 2, 2, 0, 0, 0,    0, 0, 3, 4, 0, 0, 0,    0, 0, 2, 2, 2, 2, 2,    0, 0, 0, 0, 0, 0, 0,    0, 0, 0, 0, 3, 0, 0,    0, 0, 4, 4, 0, 0, 0,    0, 0, 0, 0, 0, 0, 0,},//8
                                    { 1, 1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,   1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,    1, 1, 1, 1, 1, 1, 1,    1, 0, 0, 0, 0, 0, 0,},//9
                                }, 64);
            toraT = Content.Load<Texture2D>("tiger");
            toraR = new Rectangle(30, 30, 40, 40);
            tora = new Tiger(toraT, toraR, Color.White);

        }


        protected override void UnloadContent()
        {
            
        }

        
        protected override void Update(GameTime gameTime)
        {
           

            Keys[] pressed = Keyboard.GetState().GetPressedKeys();

            switch (gameMode1)
            {
                case GameMode.InGame:
                    {
                        foreach (Keys k in pressed)
                        {
                            camera.camMove(k);
                        }

                        if (Mouse.GetState().LeftButton == ButtonState.Pressed && Keyboard.GetState().IsKeyDown(Keys.A) && ballCount != 0 && ballDelay == false)
                        {
                            ballCount -= 1;
                            ballDelay = true;

                            ballSoundControl = ballSound.CreateInstance();
                            ballSoundControl.Play();

                            Texture2D ballTexture = Content.Load<Texture2D>("Bullet\\ball1");
                            PlayerBullet p = new PlayerBullet(ballTexture, new Rectangle(player.endRect.X, player.endRect.Y + 40, ballTexture.Width, ballTexture.Height), Color.White);
                            p.checkL = true;
                            ball.Add(p);
                        }
                        if (Mouse.GetState().LeftButton == ButtonState.Pressed && Keyboard.GetState().IsKeyDown(Keys.D) && ballCount != 0 && ballDelay == false)
                        {

                            ballCount -= 1;
                            ballDelay = true;

                            ballSoundControl = ballSound.CreateInstance();
                            ballSoundControl.Play();

                            Texture2D ballTexture = Content.Load<Texture2D>("Bullet\\ball1");
                            PlayerBullet p = new PlayerBullet(ballTexture, new Rectangle(player.endRect.X + 25, player.endRect.Y + 45, ballTexture.Width, ballTexture.Height), Color.White);
                            p.checkR = true;
                            ball.Add(p);
                        }
              
                        
                        if ( Mouse.GetState().LeftButton == ButtonState.Pressed && ballCount != 0 && ballDelay == false)
                        {

                            ballCount -= 1;
                            ballDelay = true;

                            ballSoundControl = ballSound.CreateInstance();
                            ballSoundControl.Play();

                            Texture2D ballTexture = Content.Load<Texture2D>("Bullet\\ball1");
                            PlayerBullet p = new PlayerBullet(ballTexture, new Rectangle(player.endRect.X + 25, player.endRect.Y + 45, ballTexture.Width, ballTexture.Height), Color.White);
                            p.checkR = true;
                            ball.Add(p);
                        }
                        if (Mouse.GetState().LeftButton == ButtonState.Released && !Keyboard.GetState().IsKeyDown(Keys.D) || Mouse.GetState().RightButton == ButtonState.Pressed && !Keyboard.GetState().IsKeyDown(Keys.D) ||
                            Mouse.GetState().MiddleButton == ButtonState.Pressed)
                        {

                            ballDelay = false;
                        }
                        //missile reload
                        if (Keyboard.GetState().IsKeyDown(Keys.R))
                        {
                            ballCount = 10;
                        }
                        foreach (PlayerBullet bullet in ball)
                        {
                            bullet.bulletMove();
                           
                        }
                        foreach(PlayerBullet bullet in ball)
                        {
                            if (bullet.BallRectangle.Intersects(enemy.enemyEndRect))
                            {
                                ball.Remove(bullet);
                                score.score += 20;
                                break;
                            }
                           
                        }
                        //foreach (PlayerBullet bullet in ball)
                        //{
                        //    foreach (Box b in box)
                        //{
                        //    if (bullet.BallRectangle.Intersects(b.boxRect)) ;
                        //    {
                        //        score.score += 30;
                        //        //box.Remove(b);
                        //        ball.Remove(bullet);
                        //    }
                        //}

                        //ENEMY
                        //if (enemy.enemyEndRect.Intersects(player.endRect))
                        //{
                        //    gameMode1 = GameMode.GameOver;
                        //    score.score -= 5;

                        //}

                       
                        enemy.Update(gameTime);
                        player.Update(gameTime);
                        tora.Update(gameTime);

                        foreach (CollisionFloor floor in map.CollisionFloor)
                        {
                            player.Collision(floor.FloorRectangle, map.Width, map.Height);
                            tora.Collision(floor.FloorRectangle, map.Width, map.Height);
                        }

                       
                     
                        break;
                    }
                case GameMode.Menu:
                    {
                        cursorRect.Location = new Point(Mouse.GetState().X, Mouse.GetState().Y);

                        if (menu.playRect.Intersects(cursorRect) &&Mouse.GetState().LeftButton == ButtonState.Pressed)
                        {
                            gameMode1 = GameMode.InGame;
                        }
                        if (menu.controlRect.Intersects(cursorRect) && Mouse.GetState().LeftButton == ButtonState.Pressed)
                        {
                            gameMode1 = GameMode.Controls;
                        }
                        menu.Update(gameTime);
                        break;
                    }
                case GameMode.GameOver:
                    {
                        
                        break;
                    }
                case GameMode.Controls:
                    {
                        if(Keyboard.GetState().IsKeyDown(Keys.Escape))
                        {
                            gameMode1 = GameMode.Menu;
                        }
                        break;
                    }

            }
            


            base.Update(gameTime);

        }

       

         
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, camera.getCamPos());

            switch(gameMode1)
            {
                case GameMode.InGame:
                    {
                        background.Draw(spriteBatch);
                       
                        foreach (PlayerBullet bullet in ball)
                        {
                            spriteBatch.Draw(bullet.BallTexture, bullet.BallRectangle, bullet.BallColor);
                        }
                        score.Draw(spriteBatch);
                        player.Draw(spriteBatch);
                        enemy.Draw(spriteBatch);
                        map.Draw(spriteBatch);
                        spriteBatch.Draw(tora.TigerTexture, tora.TigerRectangle, Color.White);


                        break;
                    }
                case GameMode.Menu:
                    {
                        
                        menu.Draw(spriteBatch);
                        spriteBatch.Draw(cursorTexture, cursorRect, Color.White);
                        break;
                    }
                case GameMode.GameOver:
                    {
                        spriteBatch.Draw(gameOverTxt, new Rectangle(player.getPlayerPos.X - 300, 0, gameOverTxt.Width, gameOverRect.Height), Color.White);
                        break;
                    }
                case GameMode.Controls:
                    {
                        spriteBatch.Draw(controlBg , Vector2.Zero, Color.White);
                        break;
                    }
            }
           

            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
